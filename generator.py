"""
For all directories that have a requirements.txt file in them,
go ahead and generate a dynamic child pipeline that runs 
Dependency Scanning.
"""
from jinja2 import Template
import os


def main():
    file_declaring_app_folder = 'requirements.txt'

    context = {
        'paths': [
            x[2:]
            for x, y, z in os.walk('.')
            if not x.startswith('./.') and file_declaring_app_folder in z and x[2:]
        ]
    }
    with open('dynamic.yml.j2', 'r') as f:
      tm = Template(f.read())
    with open('dynamic.yml', 'w') as f:
        f.write(tm.render(**context))


if __name__ == '__main__':
    main()
